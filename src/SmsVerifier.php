<?php

declare(strict_types=1);

use Twilio\Rest\Client;

class SmsVerifier
{
    private const STATUS_PENDING = 'pending';
    private const STATUS_APPROVED = 'approved';
    private const VERIFICATION_TYPE = 'sms';

    private Client $twilio;

    public function __construct(private string $sid, private string $token, private string $serviceId)
    {
        $this->twilio = new Client($sid, $token);
        $this->service = $this->twilio->verify->v2->services($this->serviceId);
    }

    public function sendCode(int $phoneNumber): bool
    {
        $verification = $this->service->verifications;
        $result = $verification->create($this->toFormattedPhone($phoneNumber), self::VERIFICATION_TYPE);

        return self::STATUS_PENDING === $result->status;
    }

    public function verifyCode(int $phoneNumber, string $code): bool
    {
        $result = $this->service
            ->verificationChecks
            ->create($code, ['to' => $this->toFormattedPhone($phoneNumber)]);

        return self::STATUS_APPROVED === $result->status;
    }

    private function toFormattedPhone(int $phoneNumber): string
    {
        return '+'.(string) $phoneNumber;
    }
}
