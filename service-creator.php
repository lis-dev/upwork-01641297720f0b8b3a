<?php

require_once './vendor/autoload.php';

use Twilio\Rest\Client;

$sid = getenv('TWILIO_ACCOUNT_SID');
$token = getenv('TWILIO_AUTH_TOKEN');
$serviceName = getenv('TWILIO_SERVICE_NAME');

$twilio = new Client($sid, $token);
$service = $twilio->verify->v2->services->create($serviceName ?: 'Phone Verify Service');

echo $service->sid.PHP_EOL;
