<?php

require_once './vendor/autoload.php';

$sid = getenv('TWILIO_ACCOUNT_SID');
$token = getenv('TWILIO_AUTH_TOKEN');
$serviceId = getenv('TWILIO_SERVICE_ID');

if (!$sid || !$token) {
    exit("Application can't be run without TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN env variables");
}

if (!$serviceId) {
    exit("Application can't be run without TWILIO_SERVICE_NAME env variable. Use service-creator script to generate TWILIO_SERVICE_NAME value variable");
}

$phone = (int) $_POST['phone'] ?? 0;
$code = $_POST['code'] ?? '';

$result = $phoneForm = $codeForm = '';

if ($phone) {
    $verifier = new SmsVerifier($sid, $token, $serviceId);
    if ($code) {
        $result = $verifier->verifyCode($phone, $code)
            ? '<div class="alert alert-success" role="alert">Code is valid! Congrats!</div>'
            : '<div class="alert alert-danger" role="alert">Code is invalid!</div>';
    } else {
        $result = $verifier->sendCode($phone)
            ? '<div class="alert alert-success" role="alert">Code was sent, check your phone</div>'
            : '<div class="alert alert-danger" role="alert">Something happend, code was\'t sent</div>';
    }
}

    $phoneForm = <<<HTML
        <div class="form-group">
            <label for="inputPhone1">Your phone</label>
            <input type="phone" name="phone" class="form-control" id="inputPhone1" placeholder="Enter phone" value="$phone">
            <small id="phoneHelp" class="form-text text-muted">We'll never share your phone with anyone else.</small>
        </div>
    HTML;


if ($phone) {
    $codeForm = <<<HTML
        <div class="form-group">
            <label for="inputCode1">Enter code from the sms</label>
            <input type="text" name="code" class="form-control" id="inputCode1" placeholder="Enter code">
            <small id="codeHelp" class="form-text text-muted">Twilio have just sent sms with code to you. Please, wait for it and enter it here after you got it</small>
        </div>
    HTML;
}

echo <<<HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DEMO SMS VERIFYING</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body class="container">
<form method="POST">
  $phoneForm
  $codeForm
  $result
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>
HTML;
